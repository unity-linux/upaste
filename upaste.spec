%define debug_package %{nil}

Name:           upaste
Version:        1.0.0
Release:        1%{?dist}
Summary:        Command line tool to upload things to unitylinux.com/upaste

License:        GPL
URL:            http://github.com/benapetr/stikkit
Source0:        %{name}-%{version}.tar.xz
Source1:        default_apikey
Source2:        default_url
Source3:        default_expiry
Source4:        README.Unity-Linux


Patch1:         upaste-support-private.patch
Patch2:         upaste-useragent.patch
Patch3:         upaste-lastargasfilename.patch

BuildRequires:  libcurl-devel cmake gcc-c++

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Command line tool to upload things to the Unity-Linux pastebin service
located at http://unitylinux.com/upaste.  Will accept input from a named file
piped in via standard input.  Permits the user to specify title; authors name;
expiry time; language/format and mark the paste as private.  May also be used
for any other stikked-based pastebin service.

%prep
%setup -q

%patch1 -p1
%patch2 -p1
%patch3 -p1

%build
cmake .
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot} INSTALL="install -p"
mkdir -p %{buildroot}%{_bindir}
mv %{buildroot}/usr/local/bin/upaste %{buildroot}%{_bindir}/upaste
mkdir -p %{buildroot}%{_sysconfdir}/upaste
cp %{SOURCE1} %{buildroot}%{_sysconfdir}/upaste/apikey
cp %{SOURCE2} %{buildroot}%{_sysconfdir}/upaste/url
cp %{SOURCE3} %{buildroot}%{_sysconfdir}/upaste/expiry

install -m 0644 %{SOURCE4} $RPM_BUILD_DIR/%{name}-%{version}/

%post

%preun

%files
%doc README.md License.txt README.Unity-Linux
%{_bindir}/%{name}
%dir %{_sysconfdir}/%{name}
%config %{_sysconfdir}/%{name}/apikey
%config %{_sysconfdir}/%{name}/url
%config %{_sysconfdir}/%{name}/expiry

%changelog
