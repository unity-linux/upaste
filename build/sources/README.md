upaste
=======

is a command line tool to upload paste to [stikked](https://github.com/claudehohl/Stikked) based pastebins.
Based on [stikkit](https://github.com/benapetr/stikkit)

Features
========

* Super easy to use
* Remembers stikked URL
* Automatically url encodes all data
* Automatically figures out author name if not provided explicitly (uses system name)

Installing on Unity-Linux
==============================

Should be installed as a tool by default, but if not:
```
sudo dnf install upaste
```

That's all

Building on linux
=================

Checkout this repository
```
cd upaste
cmake .
make
sudo make install
```

NOTE: you need to have `libcurl-dev`, `cmake` and `gcc-c++` installed in your system

Usage
=====

See `upaste --help` for detailed help about options.

```
upaste -b url
# now type text and hit ctrl+d to exit
cat file | upaste -b url
echo "Hello world" | upaste
```

url is an url to stikked server for example, if your server is http://something.blah/stikked and your api is http://something.blah/stikked/api then use http://something.blah/stikked as parameter to -b

By default the url parameter is stored to a configuration file (upaste will ask you though if there is none in config file)

Configuration
=====

The default configuration pulls the URL from `/etc/upaste/url` and there are default settings that will work just fine - however, a better approach might be
to create your own config directory.

The upaste software will look for `.upaste` in your home directory. Within this directory, there are a few key files worth creating.

 - *url* - Contains your base installation URL for stikked
 - *apikey* - If your stikked installation requires an API key, save it in here
 - *expiry* - The default expiry time (in minutes) for a paste, if needed
 - *author* - The default author name if you wish to use one
 - *private* - If you want to mark the paste as private (not visible in recent pastes), create this empty file

Tips
=====

Do you have a server and want to setup default pastebin site for all users in system?

Create `/etc/upaste/url` which contains the url of default pastebin site. Users will be able to override it, but by default everyone on system will use this